## Mapped experiments

### Wild-type / blumenkohl
[Figure 4B-C](mapped_experiments/blumenkohl.py)    
[Figure 4D](mapped_experiments/blu_stimulus_maps.py)  

### Lakritz
[Figure 4B-C](mapped_experiments/lakritz.py)    
[Figure 4D](mapped_experiments/lak_stimulus_maps.py)  

## Virtual prey experiment

[Figure 4F](virtual_prey/virtual_prey_example.py)  
[Figure 4G-H](virtual_prey/virtual_prey_stats.py)
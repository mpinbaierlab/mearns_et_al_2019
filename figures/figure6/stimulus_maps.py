from plotting import *
from plotting.stimulus_maps import smoothed_histogram
from datasets.side_view import experiment
import numpy as np


stimulus_map_directory = os.path.join(experiment.subdirs['analysis'], 'paramecium_positions')

averages = np.load(os.path.join(stimulus_map_directory, 'cluster_averages.npy'))
histograms = np.load(os.path.join(stimulus_map_directory, 'cluster_histograms.npy'))
random_histogram = np.load(os.path.join(stimulus_map_directory, 'random_histogram.npy'))
random_average = np.load(os.path.join(stimulus_map_directory, 'random_average.npy'))

ymin, ymax = 10, 90
xmin, xmax = 30, 190
sigma = 1.5

random_histogram = random_histogram[ymin:ymax, xmin:xmax]
random_average = random_average[ymin:ymax, xmin:xmax]
random_histogram = smoothed_histogram(random_histogram, random_average, threshold=5, sigma=sigma)


fig, axes = plt.subplots(2, 1)

for i in range(2):

    cluster_histogram = histograms[i, ymin:ymax, xmin:xmax]
    cluster_average = averages[i, ymin:ymax, xmin:xmax]

    cluster_histogram = smoothed_histogram(cluster_histogram, cluster_average, threshold=5, sigma=sigma)
    cluster_density = (cluster_histogram - random_histogram)

    lower = np.percentile(cluster_density, 85)
    upper = max([cluster_density.max(), 1e-4])
    cluster_density[cluster_density < lower] = 0

    axes[i].imshow(cluster_average, cmap='binary_r', vmin=0, vmax=255, interpolation='bilinear')
    axes[i].contourf(cluster_density, cmap='magma', levels=np.linspace(2e-5, upper, 50))

    for ax in axes:
        ax.axis('off')

# plt.show()
save_fig(fig, 'figure6', 'stimulus_maps')

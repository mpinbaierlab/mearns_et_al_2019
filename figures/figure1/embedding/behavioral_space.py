from plotting import *
from plotting.plots.tail import generate_reconstructed_points, plot_reconstructed_points
from plotting.colors import isomap_colors

from datasets.main_dataset import experiment

from behaviour_mapping.analysis.bouts import BoutData
from behaviour_mapping.analysis.alignment import dtw

from matplotlib.patches import Circle
import numpy as np
import pandas as pd


isomap = np.load(os.path.join(experiment.subdirs['analysis'], 'isomap.npy'))[:, :3]

exemplars = pd.read_csv(os.path.join(experiment.subdirs['analysis'], 'exemplars.csv'),
                        dtype={'ID': str, 'video_code': str})
exemplars = exemplars[exemplars['clean']]
exemplars = exemplars.reset_index(drop=True)

mapped_bouts = pd.read_csv(os.path.join(experiment.subdirs['analysis'], 'mapped_bouts.csv'),
                           index_col='bout_index',
                           dtype={'ID': str, 'video_code': str})
n_states = len(isomap)
cluster_sizes = np.array([(mapped_bouts['state'] == l).sum() for l in np.arange(n_states)])


if __name__ == "__main__":

    # ---------------------------------------
    # Isolate upper and lower areas of isomap
    # ---------------------------------------

    lower_limit = 50
    upper_limit = 100

    in_lower = (isomap[:, 2] <= lower_limit)
    in_upper = (isomap[:, 2] >= upper_limit)

    # -----------------------------------------------------
    # Generate paths through different regions of the space
    # -----------------------------------------------------

    # Main ring
    n1 = 12
    rx1 = 380
    ry1 = 600
    th1 = np.linspace(-3*np.pi/2, np.pi/2, n1+1)[:-1]
    x1 = rx1 * np.cos(th1) + 20
    y1 = ry1 * np.sin(th1)

    # Strike zone
    n2 = 3
    ry2 = 200
    rz2 = 200
    th2 = np.linspace(-3*np.pi/2, np.pi/2, n2+1)[:-1]
    y2 = ry2 * np.cos(th2) - 800
    z2 = rz2 * np.sin(th2) + 500

    # Fast turns
    x3 = [700] * 3
    y3 = np.linspace(-200, 600, 3)

    # ----------------------------
    # Find nearest points to paths
    # ----------------------------

    xy1 = np.array([x1, y1]).T
    nearest1 = np.array([np.argmin(np.linalg.norm(isomap[in_lower][:, :2] - p, axis=1)) for p in xy1])

    yz2 = np.array([y2, z2]).T
    nearest2 = np.array([np.argmin(np.linalg.norm(isomap[in_upper][:, [0, 2]] - p, axis=1)) for p in yz2])

    xy3 = np.array([x3, y3]).T
    nearest3 = np.array([np.argmin(np.linalg.norm(isomap[in_upper][:, :2] - p, axis=1)) for p in xy3])

    # ----------------------------------------------------
    # Import exemplar data corresponding to example points
    # ----------------------------------------------------

    examples_1 = exemplars[in_lower].iloc[nearest1]
    examples_2 = exemplars[in_upper].iloc[nearest2]
    examples_3 = exemplars[in_upper].iloc[nearest3]

    bout_idxs_1 = examples_1.index
    bout_idxs_2 = examples_2.index
    bout_idxs_3 = examples_3.index

    example_data = pd.concat([examples_1, examples_2, examples_3])

    exemplar_bouts = BoutData.from_directory(example_data, experiment.subdirs['kinematics'],
                                             check_tail_lengths=False, tail_columns_only=True)

    eigenfish = np.load(os.path.join(experiment.subdirs['analysis'], 'behaviour_space', 'eigenfish.npy'))
    mean, std = np.load(os.path.join(experiment.subdirs['analysis'], 'behaviour_space', 'tail_statistics.npy'))

    transformed_exemplars = exemplar_bouts.map(eigenfish, True, mean, std)
    init_bout = transformed_exemplars.get_bout(bout_index=bout_idxs_1[0]).values[:, :3]

    # ==============
    # MAIN PLOT HERE
    # ==============

    scale = 0.85

    # Inset bout plot setup
    ymin, ymax = -70, 20
    ax_size = (ymax - ymin)
    radius = 40

    # Main axis setup
    min1, max1 = -1500, 1300
    min2, max2 = -1200, 1600
    assert (max1 - min1) == (max2 - min2)
    main_size = float(max1 - min1)
    ax_size = 4 * ax_size / main_size

    # Create main axis
    fig = plt.figure(figsize=(4 * scale, 4 * scale))
    ax_main = fig.add_axes((0, 0, 1, 1), xlim=(min1, max1), ylim=(min2, max2), autoscale_on=False)
    xticks = [-1200, 0, 1200]
    ax_main.set_xticks(xticks)
    ax_main.set_xticklabels(xticks, fontproperties=largefont)
    ax_main.set_xticks(np.arange(-1200, 1400, 200), minor=True)
    yticks = [-1000, 0, 1000]
    ax_main.set_yticks(yticks)
    ax_main.set_yticklabels(yticks, fontproperties=largefont)
    ax_main.set_yticks(np.arange(-1000, 1600, 200), minor=True)
    open_ax(ax_main)
    ax_main.spines['bottom'].set_bounds(-1200, 1200)
    ax_main.spines['left'].set_bounds(-1000, 1400)
    ax_main.set_xlabel('Isomap dimension 1', fontproperties=largefont)
    ax_main.set_ylabel('Isomap dimension 2', fontproperties=largefont)

    # Plot points
    zorder = np.argsort(isomap[:, 2])
    ax_main.scatter(*isomap[zorder, :2].T, c=isomap_colors.colors[zorder], edgecolor=isomap_colors.ecolors[zorder],
                    lw=0.5 * scale, marker='o', s=cluster_sizes[zorder] * 0.2 * (scale ** 2))

    # Size legend
    ax_main.scatter([-1300, -1200, -1100, -1000], [1000, 1000, 1000, 1000],
                    s=np.array([3, 10, 50, 100]) * 0.2 * (scale ** 2),
                    lw=0.5 * scale, marker='o',
                    c='0.5', edgecolor='0.3')
    ax_main.text(-1250, 900, '3 10 50 100', fontproperties=verysmallfont)
    ax_main.text(-1300, 1050, 'Number of bouts', fontproperties=verysmallfont)

    # Main ring

    ring_colors = isomap_colors.colors[in_lower][nearest1]

    r1 = 550.  # rx1 * 1.5
    r2 = 800.  # ry1 * 1.3

    thetas_deltas = np.linspace(-3 * np.pi / 2, np.pi / 2, 721)
    x_deltas = r1 * np.cos(thetas_deltas) + 20
    y_deltas = r2 * np.sin(thetas_deltas)
    xy_deltas = np.array([x_deltas, y_deltas]).T
    arc_deltas = np.linalg.norm(np.diff(xy_deltas, axis=0), axis=1)
    cum_deltas = np.cumsum(arc_deltas)
    perimeter_arc = cum_deltas[-1] / 12.
    arc_lengths = [perimeter_arc * s for s in np.arange(12)]
    nearest_xy = np.array([np.argmin(np.abs(cum_deltas - l)) for l in arc_lengths])
    ax_pos = xy_deltas[nearest_xy]

    for p, bout_index, color in zip(ax_pos, bout_idxs_1, ring_colors):

        bout = transformed_exemplars.get_bout(bout_index=bout_index).values[:, :3]
        if dtw(init_bout, -bout) < dtw(init_bout, bout):
            bout *= -1
        k = np.dot(bout, eigenfish[:3]) * std + mean
        ps = generate_reconstructed_points(k, 90)
        xmin = ps[:, 0, :].min(axis=0).min(axis=-1)
        xmax = ps[:, 0, :].max(axis=0).max(axis=-1)
        s = 90. / (xmax - xmin)
        o = (xmin + xmax) / 2.
        xmin = o + (xmin - o) * s
        xmax = o + (xmax - o) * s

        l, b = ((p - (min1, min2)) / main_size) - (ax_size / 2.)

        ax = fig.add_axes((l, b, ax_size, ax_size), xlim=(xmin, xmax), ylim=(ymin, ymax), frame_on=False, xticks=[],
                          yticks=[], autoscale_on=False)

        centre = (xmin + 2.5 + radius, ymin + 2.5 + radius)
        ax.add_patch(Circle(centre, radius, linewidth=scale, facecolor=(1, 1, 1, 0.8), edgecolor=color))

        plot_reconstructed_points(ax, ps, c_lim=(0.05, 0.18), lw=scale)

    # LHS

    r3, r4 = np.array([r1, r2]) * 1.7
    x_deltas = r3 * np.cos(thetas_deltas) + 20
    y_deltas = r4 * np.sin(thetas_deltas)
    xy_deltas = np.array([x_deltas, y_deltas]).T
    arc_deltas = np.linalg.norm(np.diff(xy_deltas, axis=0), axis=1)
    cum_deltas = np.cumsum(arc_deltas)
    perimeter_arc = cum_deltas[-1] / 36.
    arc_lengths = [perimeter_arc * s for s in [12, 10, 8]]
    nearest_xy = np.array([np.argmin(np.abs(cum_deltas - l)) for l in arc_lengths])
    ax_pos = xy_deltas[nearest_xy]

    left_colors = isomap_colors.colors[in_upper][nearest2]

    for p, bout_index, color in zip(ax_pos, bout_idxs_2, left_colors):

        bout = transformed_exemplars.get_bout(bout_index=bout_index).values[:, :3]
        if dtw(init_bout, -bout) < dtw(init_bout, bout):
            bout *= -1
        k = np.dot(bout, eigenfish[:3]) * std + mean
        ps = generate_reconstructed_points(k, 90)
        xmin = ps[:, 0, :].min(axis=0).min(axis=-1)
        xmax = ps[:, 0, :].max(axis=0).max(axis=-1)
        s = 90. / (xmax - xmin)
        o = (xmin + xmax) / 2.
        xmin = o + (xmin - o) * s
        xmax = o + (xmax - o) * s

        l, b = ((p - (min1, min2)) / main_size) - (ax_size / 2.)
        ax = fig.add_axes((l, b, ax_size, ax_size), xlim=(xmin, xmax), ylim=(ymin, ymax), frame_on=False, xticks=[],
                          yticks=[], autoscale_on=False)

        centre = (xmin + 2.5 + radius, ymin + 2.5 + radius)
        ax.add_patch(Circle(centre, radius, linewidth=scale, facecolor=(1, 1, 1, 0.8), edgecolor=color))

        plot_reconstructed_points(ax, ps, c_lim=(0.05, 0.18), lw=scale)

    # RHS

    right_colors = isomap_colors.colors[in_upper][nearest3]

    arc_lengths = [perimeter_arc * s for s in [26, 28, 30]]
    nearest_xy = np.array([np.argmin(np.abs(cum_deltas - l)) for l in arc_lengths])
    ax_pos = xy_deltas[nearest_xy]

    for p, bout_index, color in zip(ax_pos, bout_idxs_3, right_colors):

        bout = transformed_exemplars.get_bout(bout_index=bout_index).values[:, :3]
        if dtw(init_bout, -bout) < dtw(init_bout, bout):
            bout *= -1
        k = np.dot(bout, eigenfish[:3]) * std + mean
        ps = generate_reconstructed_points(k, 90)
        xmin = ps[:, 0, :].min(axis=0).min(axis=-1)
        xmax = ps[:, 0, :].max(axis=0).max(axis=-1)
        s = 90. / (xmax - xmin)
        o = (xmin + xmax) / 2.
        xmin = o + (xmin - o) * s
        xmax = o + (xmax - o) * s

        l, b = ((p - (min1, min2)) / main_size) - (ax_size / 2.)

        ax = fig.add_axes((l, b, ax_size, ax_size), xlim=(xmin, xmax), ylim=(ymin, ymax), frame_on=False, xticks=[],
                          yticks=[], autoscale_on=False)

        centre = (xmin + 2.5 + radius, ymin + 2.5 + radius)
        ax.add_patch(Circle(centre, radius, linewidth=scale, facecolor=(1, 1, 1, 0.8), edgecolor=color))

        plot_reconstructed_points(ax, ps, c_lim=(0.05, 0.18), lw=scale)

    # plt.show()
    save_fig(fig, 'figure1', 'behavior_space')

from datasets.side_view import experiment
from paramecium_tracking import paramecium_directory
from behaviour_mapping.analysis3D.stimulus_mapping import RandomStimulusMapper
from behaviour_mapping.miscellaneous import Timer


if __name__ == "__main__":

    # Calculate stimulus maps for each fish in parallel
    timer = Timer()
    timer.start()
    mapper = RandomStimulusMapper(100, experiment, paramecium_directory, n_threads=10)
    mapper.run()
    total_time = timer.stop()
    print 'Total time:', timer.convert_time(total_time)

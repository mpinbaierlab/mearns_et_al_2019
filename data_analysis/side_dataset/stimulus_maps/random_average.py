from paramecium_tracking import paramecium_directory
from behaviour_mapping.analysis.stimulus_mapping import find_paramecia
from behaviour_mapping.miscellaneous import Timer
import numpy as np
import os


if __name__ == "__main__":

    timer = Timer()
    timer.start()

    # Compute average
    rand = np.load(os.path.join(paramecium_directory, 'random_frames.npy')).astype('float64')
    average = np.mean(rand, axis=0)
    np.save(os.path.join(paramecium_directory, 'random_average.npy'), average)

    # Compute histogram
    histogram = np.zeros((100, 250))
    for img in rand:
        img_points, img_orientations = find_paramecia(img, image_threshold=0, size_threshold=0)
        img_counts, x, y = np.histogram2d(*img_points.T, bins=[np.arange(0, 251), np.arange(0, 101)])
        histogram += img_counts.T
    np.save(os.path.join(paramecium_directory, 'random_histogram.npy'), histogram)

    print 'Time taken:', timer.convert_time(timer.stop())

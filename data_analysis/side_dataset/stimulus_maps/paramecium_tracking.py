from datasets.side_view import experiment
from behaviour_mapping.manage_files import create_folder
import os
import pandas as pd
from behaviour_mapping.analysis3D.stimulus_mapping import JawStimulusMapper
from behaviour_mapping.miscellaneous import Timer


paramecium_directory = create_folder(experiment.subdirs['analysis'], 'paramecium_positions')


if __name__ == "__main__":

    jaw_movements = pd.read_csv(os.path.join(experiment.subdirs['analysis'], 'mapped_jaw.csv'),
                                index_col=0,
                                dtype={'ID': str, 'video_code': str})

    bouts = pd.read_csv(os.path.join(experiment.subdirs['analysis'], 'mapped_bouts.csv'),
                        index_col=0,
                        dtype={'ID': str, 'video_code': str})
    bouts_by_video = bouts.groupby('video_code')

    nearest_bouts = []
    last_nearest = -1
    for idx, jaw_info in jaw_movements.iterrows():
        bout_starts = bouts_by_video.get_group(jaw_info.video_code)['start']
        t_diff = bout_starts - jaw_info.start
        preceding = bout_starts[t_diff < 0]
        bout_idx = None
        if (len(preceding) > 0) and (preceding.idxmax() != last_nearest):
            last_nearest = preceding.idxmax()
            bout_idx = last_nearest
        nearest_bouts.append(bout_idx)
    jaw_movements['nearest_bout'] = nearest_bouts
    jaw_movements = jaw_movements[~jaw_movements['nearest_bout'].isnull()]
    jaw_movements['bout_start'] = bouts.loc[jaw_movements['nearest_bout'].apply(int).values, 'start'].values
    jaw_movements = jaw_movements[(jaw_movements['bout_start'] - jaw_movements['start']) > -200]

    # Calculate stimulus maps for each fish in parallel
    timer = Timer()
    timer.start()
    mapper = JawStimulusMapper(jaw_movements, experiment, paramecium_directory, n_threads=10)
    mapper.run()
    total_time = timer.stop()
    print 'Total time:', timer.convert_time(total_time)

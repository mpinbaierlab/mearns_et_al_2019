from paramecium_tracking import paramecium_directory
from datasets.side_view import experiment
from behaviour_mapping.analysis.stimulus_mapping import find_paramecia
import pandas as pd
import numpy as np
import os


if __name__ == "__main__":

    n_clusters = 2
    cluster_histograms = np.zeros((n_clusters, 100, 250))
    cluster_average_maps = [[] for i in range(n_clusters)]
    cluster_fish_counts = [[] for i in range(n_clusters)]

    for idx, fish_info in experiment.data.iterrows():
        ID = fish_info.ID
        print ID
        fish_maps = np.load(os.path.join(paramecium_directory, ID + '.npy'))
        events = pd.read_csv(os.path.join(paramecium_directory, ID + '.csv'))
        assert len(fish_maps) == len(events)

        for i in range(n_clusters):
            cluster_maps = fish_maps[(events['cluster'] == i)]
            # Add points to the histogram
            for frame in cluster_maps:
                img_points, img_orientations = find_paramecia(frame, image_threshold=0, size_threshold=0)
                img_counts, x, y = np.histogram2d(*img_points.T, bins=[np.arange(0, 251), np.arange(0, 101)])
                cluster_histograms[i] += img_counts.T

            # Calculate fish average
            fish_counts = float(len(cluster_maps))
            if fish_counts > 0:
                average_map = np.mean(cluster_maps.astype('float64'), axis=0)
            else:
                average_map = np.zeros(cluster_maps.shape[1:])
            cluster_average_maps[i].append(average_map)
            cluster_fish_counts[i].append(fish_counts)

    np.save(os.path.join(paramecium_directory, 'cluster_histograms.npy'), cluster_histograms)

    cluster_average_maps = np.array(cluster_average_maps)  # (n_modules, n_fish, x, y)
    cluster_fish_counts = np.array(cluster_fish_counts)  # (n_modules, n_fish)
    cluster_fish_counts /= cluster_fish_counts.sum(axis=1, keepdims=True)
    cluster_averages = np.einsum('ij,ijkl->ikl', cluster_fish_counts, cluster_average_maps)
    np.save(os.path.join(paramecium_directory, 'cluster_averages.npy'), cluster_averages)

from paths import *
from behaviour_mapping.analysis.embedding import IsomapPrecomputed
import numpy as np
from scipy.spatial.distance import squareform


if __name__ == "__main__":

    jaw_distance_matrix = np.load(paths['distance_matrix'])
    jaw_distance_matrix = squareform(jaw_distance_matrix)

    isomap = IsomapPrecomputed(n_components=20, n_neighbors=5)

    isomap_embedding = isomap.fit_transform(jaw_distance_matrix)
    # for i in (0, 2):  # mirror axes 0 and 2
    #     isomap_embedding[:, i] *= -1
    kernel_pca_eigenvalues = isomap.kernel_pca_.lambdas_
    reconstruction_errors = isomap.reconstruction_errors()

    np.save(paths['jaw_isomap'], isomap_embedding)
    np.save(paths['kernel_pca_eigenvalues'], kernel_pca_eigenvalues)
    np.save(paths['reconstruction_errors'], reconstruction_errors)

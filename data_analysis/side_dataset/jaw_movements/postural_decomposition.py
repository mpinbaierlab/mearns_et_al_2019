from paths import *
from behaviour_mapping.analysis3D.jaw_analysis import JawData
from behaviour_mapping.miscellaneous import print_heading, Timer
from behaviour_mapping.analysis.alignment import calculate_distance_matrix
import pandas as pd
import numpy as np
import os


if __name__ == "__main__":

    jaw_movements = pd.read_csv(os.path.join(experiment.directory, 'jaw_movements.csv'),
                                dtype={'ID': str, 'video_code': str})

    jaw_data = JawData.from_directory(jaw_movements, experiment.subdirs['kinematics'], check_magnitude=True)
    jaw_data.metadata.index.name = 'event_index'
    jaw_data.metadata.to_csv(paths['mapped_jaw'], index=True)

    transformed, pca = jaw_data.transform()

    eigenjaw = pca.components_
    jaw_statistics = np.array([jaw_data.mean.values, jaw_data.std.values])
    explained_variance = pca.explained_variance_ratio_

    np.save(paths['eigenjaw'], eigenjaw)
    np.save(paths['jaw_statistics'], jaw_statistics)
    np.save(paths['explained_variance'], explained_variance)

    print_heading('CALCULATING DISTANCE MATRIX')
    jaw_transformed = transformed.list_events(values=True, ndims=1)
    lengths = [len(trace) for trace in jaw_transformed]
    min_length = min(lengths)
    jaw_transformed = [trace[:min_length] for trace in jaw_transformed]

    timer = Timer()
    timer.start()
    distance_matrix = calculate_distance_matrix(jaw_transformed, fs=400.)
    np.save(paths['distance_matrix'], distance_matrix)
    print 'Time taken: {}'.format(timer.convert_time(timer.stop()))

from paths import *
import numpy as np
import pandas as pd
from hdbscan import HDBSCAN


if __name__ == "__main__":

    jaw_df = pd.read_csv(paths['mapped_jaw'], index_col=0, dtype={'ID': str, 'video_code': str})
    isomap = np.load(paths['jaw_isomap'])[:, :2]
    jaw_cluster_labels = HDBSCAN(min_samples=1, min_cluster_size=100).fit_predict(isomap)
    jaw_df['cluster'] = jaw_cluster_labels
    jaw_df.to_csv(paths['mapped_jaw'], index=True)

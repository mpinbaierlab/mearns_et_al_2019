from datasets.side_view import experiment
from behaviour_mapping.manage_files import create_folder
import os

jaw_analysis_directory = create_folder(experiment.subdirs['analysis'], 'jaw')

paths = {}
paths['mapped_jaw'] = os.path.join(experiment.subdirs['analysis'], 'mapped_jaw.csv')

for fname in ['distance_matrix', 'eigenjaw', 'jaw_statistics', 'explained_variance',
              'jaw_isomap', 'kernel_pca_eigenvalues', 'reconstruction_errors']:
    paths[fname] = os.path.join(jaw_analysis_directory, fname + '.npy')

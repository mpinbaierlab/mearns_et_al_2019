from datasets.side_view import experiment
from datasets.main_dataset import experiment as mapping_experiment
import numpy as np
import pandas as pd
from behaviour_mapping.video import Video, video_code_to_timestamp
from behaviour_mapping.analysis.bouts import BoutData
from behaviour_mapping.analysis.alignment import calculate_distance_matrix_templates
from behaviour_mapping.analysis.interpolation import interpolate_nd
from behaviour_mapping.miscellaneous import print_heading, Timer
import os


jaw_analysis_directory = os.path.join(experiment.subdirs['analysis'], 'jaw')
mapped_jaw_path = os.path.join(experiment.subdirs['analysis'], 'mapped_jaw.csv')


if __name__ == "__main__":

    mapped_bouts = pd.read_csv(os.path.join(experiment.subdirs['analysis'], 'mapped_bouts.csv'),
                               index_col=0, dtype={'ID': str, 'video_code': str})
    mapped_jaw = pd.read_csv(mapped_jaw_path, index_col=0, dtype={'ID': str, 'video_code': str})

    captures = mapped_jaw.groupby('cluster').get_group(0)
    bouts_by_video = mapped_bouts.groupby('video_code')

    nearest_bouts = []
    jaw_onsets = []
    for video_code, video_jaw in captures.groupby('video_code'):
        bout_starts = bouts_by_video.get_group(video_code)['start']
        jaw_starts = video_jaw['start']
        for idx, jaw in jaw_starts.iteritems():
            t_diff = bout_starts - jaw
            nearest = bout_starts[t_diff < 0].idxmax()
            nearest_bouts.append(nearest)
            jaw_onsets.append(jaw)

    strike_bouts = mapped_bouts.loc[nearest_bouts]
    strike_bouts['jaw_onset'] = jaw_onsets

    strike_bouts.to_csv(os.path.join(jaw_analysis_directory, 'strike_bouts.csv'), index=True)


    # for fish_ID, fish_bouts in strike_bouts.groupby('ID'):
    #     fish_info = experiment.data[experiment.data['ID'] == fish_ID].iloc[0]
    #     for video_code, video_bouts in fish_bouts.groupby('video_code'):
    #         timestamp = video_code_to_timestamp(video_code)
    #         video_path = os.path.join(experiment.video_directory, fish_info.video_directory, timestamp + '.avi')
    #         v = Video(video_path)
    #         for idx, bout_info in video_bouts.iterrows():
    #             v.scroll(first_frame=bout_info.start, last_frame=bout_info.end)


    #########################
    n_dims = 3
    frame_rate = 400.
    mapping_frame_rate = 500.
    #########################

    # Import data from mapping experiment
    mapping_space_directory = os.path.join(mapping_experiment.subdirs['analysis'], 'behaviour_space')
    eigenfish = np.load(os.path.join(mapping_space_directory, 'eigenfish.npy'))
    mean_tail, std_tail = np.load(os.path.join(mapping_space_directory, 'tail_statistics.npy'))
    strike_info = pd.read_csv(os.path.join(mapping_experiment.subdirs['analysis'],
                                           'capture_strikes',
                                           'capture_strikes.csv'),
                                index_col='bout_index',
                                dtype={'ID': str, 'video_code': str})
    strikes = BoutData.from_directory(strike_info, mapping_experiment.subdirs['kinematics'],
                                      check_tail_lengths=False, tail_columns_only=True)
    strikes = strikes.map(eigenfish, whiten=True, mean=mean_tail, std=std_tail)
    strikes = strikes.list_bouts(values=True, ndims=n_dims)
    strikes = [strike[12:37] for strike in strikes]

    # Import experiment bouts
    experiment_strikes = BoutData.from_directory(strike_bouts, experiment.subdirs['kinematics'],
                                                 check_tail_lengths=False, tail_columns_only=True)
    experiment_strikes = experiment_strikes.map(eigenfish, whiten=True, mean=mean_tail, std=std_tail)

    # Compute distance matrices
    print_heading('CALCULATING DISTANCE MATRICES')
    distances = {}
    analysis_times = []
    timer = Timer()
    timer.start()
    for ID in experiment_strikes.metadata['ID'].unique():
        print ID + '...',
        queries = experiment_strikes.list_bouts(IDs=[ID], values=True, ndims=n_dims)
        queries = [interpolate_nd(bout, frame_rate, mapping_frame_rate) for bout in queries]
        queries = [query[12:37] for query in queries]
        fish_distances = calculate_distance_matrix_templates(queries, strikes, fs=frame_rate)
        distances[ID] = fish_distances
        time_taken = timer.lap()
        analysis_times.append(time_taken)
        print timer.convert_time(time_taken)
    if len(analysis_times) > 0:
        print 'Average time: {}'.format(timer.convert_time(timer.average))

    # Assign exemplars
    mapped_strikes = experiment_strikes.metadata.copy()
    mapped_strikes['exemplar'] = None
    mapped_strikes['embedding_error'] = None
    mapped_strikes['strike_cluster'] = None
    for ID, fish_distances in distances.iteritems():
        bout_idxs = mapped_strikes[mapped_strikes['ID'] == ID].index
        nearest_exemplar = np.argmin(fish_distances, axis=1)
        embedding_error = np.min(fish_distances, axis=1)
        strike_label = strike_info.iloc[nearest_exemplar]['strike_cluster'].values
        mapped_strikes.loc[bout_idxs, 'exemplar'] = nearest_exemplar
        mapped_strikes.loc[bout_idxs, 'embedding_error'] = embedding_error
        mapped_strikes.loc[bout_idxs, 'strike_cluster'] = strike_label
    mapped_strikes.to_csv(os.path.join(jaw_analysis_directory, 'strike_bouts.csv'), index=True)

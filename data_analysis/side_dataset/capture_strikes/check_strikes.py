from find_strikes import jaw_analysis_directory, mapped_jaw_path

from datasets.side_view import experiment
from datasets.main_dataset import experiment as mapping_experiment
import pandas as pd
import numpy as np
from behaviour_mapping.video import Video, video_code_to_timestamp
from matplotlib import pyplot as plt
import os


if __name__ == "__main__":

    strike_bouts = pd.read_csv(os.path.join(jaw_analysis_directory, 'strike_bouts.csv'), index_col=0,
                               dtype={'ID': str, 'video_code': str})

    strike_isomap = np.load(os.path.join(mapping_experiment.subdirs['analysis'],
                                         'capture_strikes',
                                         'isomapped_strikes.npy'))[:, :2]

    jaw = pd.read_csv(mapped_jaw_path, index_col=0, dtype={'ID': str, 'video_code': str})
    captures = jaw.groupby('cluster').get_group(0)

    example_bouts = strike_bouts.loc[[3187, 17042]]
    example_jaw = jaw['start'].isin(example_bouts['jaw_onset'])
    capture_examples = jaw[example_jaw]

    print capture_examples

    for idx, example in capture_examples.iterrows():
        fish_info = experiment.data[experiment.data['ID'] == example.ID].iloc[0]
        timestamp = video_code_to_timestamp(example.video_code)
        video_path = os.path.join(experiment.video_directory, fish_info.video_directory, timestamp + '.avi')
        v = Video(video_path)
        v.scroll(first_frame=example.start - 50, last_frame=example.end+50)


    # plt.hist(strike_bouts['embedding_error'], bins=np.arange(0, 250, 5))
    # plt.show()
    #
    # strike_bouts = strike_bouts[strike_bouts['embedding_error'] < 150]
    #
    # examples_1 = np.where(strike_isomap[:, 0] < -150)[0]
    # examples_1 = strike_bouts[strike_bouts['exemplar'].isin(examples_1)]
    #
    # exit_flag = 0
    # for fish_ID, fish_bouts in examples_1.groupby('ID'):
    #     fish_info = experiment.data[experiment.data['ID'] == fish_ID].iloc[0]
    #     for video_code, video_bouts in fish_bouts.groupby('video_code'):
    #         timestamp = video_code_to_timestamp(video_code)
    #         video_path = os.path.join(experiment.video_directory, fish_info.video_directory, timestamp + '.avi')
    #         v = Video(video_path)
    #         for idx, bout_info in video_bouts.iterrows():
    #             v.scroll(first_frame=bout_info.jaw_onset - 100, last_frame=bout_info.jaw_onset + 50)
    #             if v.enter():
    #                 print bout_info
    #                 exit_flag = 1
    #                 break
    #         if exit_flag:
    #             break
    #     if exit_flag:
    #         break
    #
    # examples_2 = np.where(strike_isomap[:, 0] > 0)[0]
    # examples_2 = strike_bouts[strike_bouts['exemplar'].isin(examples_2)]
    #
    # exit_flag = 0
    # for fish_ID, fish_bouts in examples_2.groupby('ID'):
    #     fish_info = experiment.data[experiment.data['ID'] == fish_ID].iloc[0]
    #     for video_code, video_bouts in fish_bouts.groupby('video_code'):
    #         timestamp = video_code_to_timestamp(video_code)
    #         video_path = os.path.join(experiment.video_directory, fish_info.video_directory, timestamp + '.avi')
    #         v = Video(video_path)
    #         for idx, bout_info in video_bouts.iterrows():
    #             v.scroll(first_frame=bout_info.jaw_onset - 100, last_frame=bout_info.jaw_onset + 50)
    #             if v.enter():
    #                 print bout_info
    #                 exit_flag = 1
    #                 break
    #         if exit_flag:
    #             break
    #     if exit_flag:
    #         break

    # plt.scatter(*strike_isomap.T, c='0.5', s=5)
    # plt.scatter(*strike_isomap[strike_bouts['exemplar'].values].T, c='r', s=10)
    # plt.show()

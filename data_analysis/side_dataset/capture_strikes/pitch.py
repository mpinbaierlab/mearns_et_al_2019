from datasets.side_view import experiment
from behaviour_mapping.analysis3D import JawData
import os
import numpy as np
import pandas as pd
from scipy.stats import ttest_ind

bouts_path = os.path.join(experiment.subdirs['analysis'], 'mapped_bouts.csv')
bouts_df = pd.read_csv(bouts_path, dtype={'ID': str, 'video_code': str})

jaw_path = os.path.join(experiment.subdirs['analysis'], 'mapped_jaw.csv')
jaw_df = pd.read_csv(jaw_path, dtype={'ID': str, 'video_code': str}, index_col=0)

by_clustter = jaw_df.groupby('cluster')

jaw = JawData.from_directory(jaw_df, experiment.subdirs['kinematics'], check_magnitude=False, elevation=True)
capture_jaw = jaw.list_events(event_idxs=by_clustter.get_group(0).index)
spontaneous_jaw = jaw.list_events(event_idxs=by_clustter.get_group(1).index)

capture_pitches = [np.degrees(nom['fish_elevation'].values[0]) for nom in capture_jaw]
spontaneous_pitches = [np.degrees(nom['fish_elevation'].values[0]) for nom in spontaneous_jaw]

bar_heights = [np.mean(spontaneous_pitches), np.mean(capture_pitches)]
stds = [np.std(spontaneous_pitches), np.std(capture_pitches)]
sems = [np.std(spontaneous_pitches) / np.sqrt(len(spontaneous_pitches)),
        np.std(capture_pitches) / np.sqrt(len(capture_pitches))]

print bar_heights
print ttest_ind(spontaneous_pitches, capture_pitches)

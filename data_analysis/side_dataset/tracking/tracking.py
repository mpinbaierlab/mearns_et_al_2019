from datasets.side_view import experiment
from behaviour_mapping.miscellaneous import print_heading


if __name__ == "__main__":

    # Update fish entries
    experiment.update_entries()

    # Perform tracking
    experiment.calculate_backgrounds()
    experiment.set_thresholds(n_points=51)
    experiment.set_ROIs()

    print_heading('Tracking')
    experiment.run_tracking(n_points=51)
    print 'Done!\n'

    print_heading('Kinematics')
    experiment.calculate_kinematics(frame_rate=400.)
    print 'Done!\n'

    # Find bouts
    experiment.set_bout_detection_thresholds(0)
    print_heading('Bouts')
    bouts = experiment.get_bouts(check_ROI=False)
    print 'Done!\n'

    # Find jaw movements
    print_heading('Jaw movements')
    experiment.get_jaw_movements()
    print 'Done!\n'

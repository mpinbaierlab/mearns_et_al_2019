# Modelling transitions between clusters

## Simplex projection

Compares Markov models of different orders to predict the next bout in a behavioral sequence.

## Significant transitions

Identifies which transitions between behavioral clusters are higher than would be expected by chance.

# Transition dynamics

Analysing transition dynamics with singular-value decomposition (SVD).

## Fish transition matrices
Computes one-step transition matrices for each fish individually and for all fish combined. Also performs smoothing of
transition matrices.

## Singular value decomposition
SVD of symmetric and antisymmetric components of smoothed transition matrix obtained by combining all fish.

## Shuffled transitions
Computes the SVD for shuffled data (cross-validated).

## Eye convergence vs. SVD
Spearman correlation between transition modes (obtained by SVD) and eye convergence states.

## Cross-validated SVD
Computes the cross-validated SVD to obtain errors on the singular value magnitudes.
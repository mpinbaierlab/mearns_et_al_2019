Analysis scripts organized by experiment.

## Main dataset

Analysis of the primary dataset used to generate the behavioral space. Data from this experiment are shown in Figure 1, 
Figure S1, Figure 2, Figure S2, Figure 3, Figure S4, and Figure 5.

## Spontaneous

Analysis of zebrafish larvae swimming in the absence of prey. Data from this experiment are shown in Figure S3.

## Mutants

Analysis of _blumenkohl_ and _lakritz_ mutants. Data from this experiment are shown in Figure 4 and Figure S6.

## Ath5 ablation

Analysis of larvae with pharmacogenetic ablation of RGCs. Data from this experiment are shown in Figure S5.

## Virtual prey

Analysis of prey capture to virtual prey objects. Data from this experiment are shown in Figure 4.

## Side dataset

Analysis of behavior filmed simultaneously from above and from the side. Data from this experiment are shown in 
Figure 6.

## Lensectomy

Analysis of larvae with lenses surgically removed. Data from this experiment are shown in Figure 7.

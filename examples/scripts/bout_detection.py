from examples.data import *
from behaviour_mapping.tracking import SetBoutDetectionThreshold, find_video_bouts
import pandas as pd
from matplotlib import pyplot as plt

# Load the data frame containing kinematic data
df = pd.read_csv(kinematics_csv)

# Set thresholds for detecting bouts
tail_tip = df.loc[:, 'tip']
thresholds = SetBoutDetectionThreshold(tail_tip)
thresh, min_length = thresholds.set()

# Find bouts
frame_rate = 500.
bout_frames = find_video_bouts(df, frame_rate, thresh, min_length)

# Plot bouts
plt.figure()
plt.plot(df.loc[:, 'tip'])
for bout in bout_frames:
    plt.plot(df.loc[bout[0]:bout[1], 'tip'], c='r')
plt.show()

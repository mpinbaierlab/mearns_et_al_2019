from examples.data import *
from behaviour_mapping.tracking import calculate_kinematics
import numpy as np
from matplotlib import pyplot as plt

# Calculate kinematics from tracking data
frame_rate = 500.
df = calculate_kinematics(tracking_csv, tracking_npy, frame_rate)

# Plot kinematics
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True)
ax1.set_xlim(df.index[0], df.index[-1])

# Plot tail data
tail_cols = [col for col in df.columns if col[0] == 'k']
tail_data = df.loc[:, tail_cols]
ax1.imshow(tail_data.T, cmap='RdBu_r', clim=(-np.pi, np.pi), interpolation='bilinear', aspect='auto')

# Plot tail tip angle
ax2.plot(df.loc[:, 'tip'])

# Plot eye angles
ax3.plot(df.loc[:, 'left'])
ax3.plot(df.loc[:, 'right'])

# Show plots
plt.show()

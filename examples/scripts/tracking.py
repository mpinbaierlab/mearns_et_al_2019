from examples.data import *
from behaviour_mapping.miscellaneous import Timer
from behaviour_mapping.tracking import track_video, check_tracking
import cv2

# Open background file
background = cv2.imread(background_file, 0)

# Tracking arguments
thresh1, thresh2 = 18, 220
n_points = 51

# Run tracking
timer = Timer()
timer.start()
print 'Tracking all frames (this may take a few minutes)...',
df, points = track_video(video_file, background, thresh1, thresh2, n_points)
time_taken = timer.stop()
print 'done! (time: {})'.format(timer.convert_time(time_taken))
print df.head(10)
print points.shape

# Show the output of the tracking
check_tracking(video_file, tracking_csv, tracking_npy)

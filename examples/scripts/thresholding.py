from examples.data import *
from behaviour_mapping.tracking import set_thresholds

# Set thresholds for tracking
thresh1, thresh2 = set_thresholds([video_file], background_file, 18, 220)
print 'Thresholds:', thresh1, thresh2

from examples.data import *
from behaviour_mapping.tracking import RegionOfInterest
import cv2

# Draw a rectangular ROI within the background image
background = cv2.imread(background_file, 0)
ROI_selector = RegionOfInterest(background)
ROI = ROI_selector.select()
print 'ROI:', ROI

from examples.data import *
from behaviour_mapping.video import Video

video = Video(video_file)

print "Press return, space or esc keys to exit."

# Play the video
video.play()

# Scroll through frames of the video
video.scroll()

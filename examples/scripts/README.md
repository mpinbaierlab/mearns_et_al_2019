Short scripts demonstrating different stages of the tracking pipeline using example data.

## video.py

Demonstrating the [Video](../../behaviour_mapping/video/video.py) class. Playing video files and scrolling through video
files using the track bar.

## background.py

Demonstrating background calculation.

## thresholding.py

Demonstrating how thresholds are set for the tracking and how changing thresholds affects tracking performance.

## tracking.py

Running tracking on the example video provided.

## region_of_interest.py

Drawing a region of interest within the background image.

## kinematics.py

Extracting kinematics (tail curvature angles, eye angles, etc.) from the raw tracking data

## bout_detection.py

Setting thresholds for detecting bouts and finding bouts in the example data provided.

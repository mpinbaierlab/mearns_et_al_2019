from examples.data import *
from behaviour_mapping.tracking import calculate_projection
import cv2

# Perform median intensity projection of every 20th frame from the example video
background = calculate_projection(video_file, projection_type='median', sample_factor=20)
background = background.astype('uint8')
cv2.imshow('background', background)
cv2.waitKey(0)
cv2.destroyAllWindows()

# Load background file
bg = cv2.imread(background_file, 0)
cv2.imshow('pre-computed background', bg)
cv2.waitKey(0)
cv2.destroyAllWindows()

Sample data.

* example_video.avi: a section of compressed video data from the main dataset. Same data shown in Figure 1C-D and 
Video S1.
* example_background.tiff: the background image for the same fish.
* example_tracking.npy: tail points extracted from the example video.
* example_tracking.csv: other tracking data (e.g. heading, eye angles) from the example video.
* example_kinematics.csv: extracted kinematic data for the example video.

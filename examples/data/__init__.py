import os

top_dir = os.path.dirname(os.getcwd())
directory = os.path.join(top_dir, 'data')

video_file = os.path.join(directory, 'example_video.avi')
background_file = os.path.join(directory, 'example_background.tiff')
tracking_csv = os.path.join(directory, 'example_tracking.csv')
tracking_npy = os.path.join(directory, 'example_tracking.npy')
kinematics_csv = os.path.join(directory, 'example_kinematics.csv')

__all__ = ('video_file', 'background_file', 'tracking_csv', 'tracking_npy', 'kinematics_csv')

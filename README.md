## About

Code for [Mearns et al.](https://doi.org/10.1016/j.cub.2019.11.022)  
_Deconstructing Hunting Behavior Reveals a Tightly Coupled Stimulus-Response Loop_

___

Primary author: Duncan Mearns  
Department Genes - Circuits - Behavior  
Max Planck Institute of Neurobiology  
mearns@neuro.mpg.de  

___

Python version 2.7

___

## Code structure

The code is organized into various folders.

#### Behaviour mapping
A collection of packages and modules written to perform tracking of freely swimming zebrafish larvae and unsupervised 
analysis of behavioral kinematics and dynamics

#### Data analysis

Analysis scripts for each experiment performed for the paper.

#### Figures

Code for making figures for the paper.

#### Datasets

Pathing and pre-processing of data used by scripts in data_analysis and figures.

#### Plotting

Various plotting functions used to help making figures.

#### Examples

Sample data and short scripts demonstrating different steps in the tracking pipeline.

___

## Data Availability

Compressed kinematic data from the main dataset are available at [Mendeley Data](https://doi.org/10.17632/mw2mmpdz3g.1).

If you wish to access the complete dataset (including raw video data, tracking output, etc.), please get in touch.

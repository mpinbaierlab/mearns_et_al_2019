# Behaviour mapping

Collection of packages related to animal tracking and unsupervised analysis of behavior.

## Experiments

Code for organising and handling experiments. Click [here](experiments/) for more info.

## Tracking

Code for animal tracking. Click [here](tracking/) for more info, or [here](tracking3D/) for additional 
info on tracking animals from the side.

## Analysis

Code for unsupervised behavioral analysis. Click [here](analysis/) for more info, or
[here](analysis3D/) for additional info on analysing jaw movements.

## Manage files

Contains helper functions for navigating directories and files.

## Miscellaneous

Contains miscellaneous helper functions such as input/output (print statements, key presses, etc.), timers, and basic
routines (splitting arrays, converting between data types, etc.).

# Multithreading

Some of the code in this project is accelerated by running in multiple threads simultaneously. For parallel code with
its own memory stacks, the [joblib](https://joblib.readthedocs.io/en/latest/parallel.html) library is used. If parallel
code requires shared memory, or implementation with joblib poses more of a challenge, the `MultiThreading` class is used
instead.
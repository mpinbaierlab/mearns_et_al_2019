Contains statistical tests and tools for:

* Energy statistics: the function `energy_statistic` compute the energy distance between two multi-dimensional datasets.

* Mann-Whitney U: contains an exact implementation of the Mann-Whitney U-test when there are fewer than 20 samples.

* Multiple comparisons: contains an implementation of the Bonferroni-Holm method for handling multiple comparisons, as
  well as a false discovery rate correction.
  
* Permutation test: contains the `PermutationTest` class providing a general, relatively fast (multi-threaded)
  implementation of a permutation test.

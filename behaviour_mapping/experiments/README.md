Tracking experiments
--------------------
The [`TrackingExperiment`](tracking_experiment_base.py) class helps to streamline the pipeline of tracking videos 
acquired using either the standard 2D prey capture assay ([`TrackingExperiment2D`](tracking_experiment_2D.py)), or 
modified 3D prey capture assay ([`TrackingExperiment3D`](tracking_experiment_3D.py)).

Each experiment is fully contained within a single directory.  
This directory has files:
* [fish_data](#fish-data) (csv file)
* [bouts](#bouts) (csv file)
* [log](#log) (text file)

And subdirectories:
* [videos](#Videos)
* [backgrounds](#Backgrounds)
* [tracking](#Tracking)
* [kinematics](#Kinematics)
* [analysis](#Analysis)

The complete path to the top-level experiment directory should be passed whenever a `TrackingExperiment` object is
initialized. A typical tracking pipeline might look something like this:

```python
from behaviour_mapping.experiments import TrackingExperiment2D

n_points = 51  # Number of points tp fit to the tail
frame_rate = 500.  # Video acquisition rate (frame per second)

# Set the path to an experiment folder
mypath = 'D:\\Data\\my_experiment'
# Initialize a TrackingExperiment object
experiment = TrackingExperiment2D(mypath)
# Open the experiment (this step opens the metadata etc.)
experiment.open()

# Perform tracking
experiment.update_entries()  # Adds new fish to the experiment
experiment.calculate_backgrounds()  # Calculates backgrounds
experiment.set_thresholds(n_points=n_points)  # Opens a GUI for setting tracking thresholds
experiment.set_ROIs()  # Opens a GUI for setting regions of interest
experiment.run_tracking(n_points=n_points)  # Performs tracking with each animal handled in a separate process
experiment.calculate_kinematics(frame_rate=frame_rate)  # Extracts kinematic data from the raw tracking
thresholds = experiment.set_bout_detection_thresholds(0)  # Opens a GUI for setting bout detection thresholds
experiment.get_bouts(frame_rate=frame_rate, *thresholds)  # Finds all putative bouts in all videos
```

## Videos

Videos are acquired using [StreamPix](https://www.norpix.com/products/streampix/streampix.php), compressed using Xvid
in [VirtualDub](http://www.virtualdub.org/) and stored in the videos directory of the experiment. As the videos can take
up quite a lot of storage space, the `TrackingExperiment` class has the option to store videos in a different location,
whose path can be passed as the _video_directory_ argument when initializing the experiment object.

Within the videos directory, each date during which experiments were performed gets its own subdirectory. Each animal
then gets its own folder within this date directory (YYYY_MM_DD), and each of these then contains all the video files
recorded for that given animal on that given date. Videos are of the format _HH-MM-SS.avi_, representing a timestamp
that the acquisition began.

## Fish data

The _fish_data.csv_ file contains the metadata for all the animals belonging to the experiment. This metadata is 
accessed through the data attribute of a `TrackingExperiment` object as a pandas DataFrame. The name of the metadata 
file can be set when the `TrackingExperiment` class is first initialized by passing a string to the _data_name_ 
argument.

In the metadata file, each row contains information for a single animal. All metadata files have columns:
* ID
* date
* name
* video_directory
* background_path
* tracking_directory
* kinematics_directory

The _ID_ is a unique identifier for a fish in the experiment. It is generated from the appropriate date and animal 
folder in the video directory. By default, the ID takes the form: YYYYMMDDNN, where NN is the fish number. A custom 
function can be used to generate the ID by passing it as an argument when the _update_entries_ method is called.

The _date_ and _name_ columns contain the date the animal was tested, and the name the animal has in its respective date
folder in the video directory (note, this is not the same as its ID).

The _video_directory_ contains a string representing a path to a given animal's video files from the top-level video
directory, i.e. a concatenation of the date folder and animal name. The full path to the video files can be obtained by
concatenating the experiment _video_directory_ and the animal _video_directory_.

The _background_path_ contains the path to the background file(s) for the animal within the experiment directory
([see below](#backgrounds)).

The _tracking_directory_ and _kinematics_directory_ contain the path to the tracking and kinematic data respectively for
each animal within the experiment directory ([see below](#tracking)).

In addition, if the condition argument is True when a `TrackingExperiment` object is first initialized, a _condition_
column will be added to the metadata. This condition column could contain, for example, whether an animal is mutant,
wild-type or heterozygous; which lens was removed in a lensectomy experiment; or whether an animal is in the ablated or
control group of an experiment.

Finally, each type of `TrackingExperiment` will have additional columns containing, e.g. thresholds and ROIs that are
specific for a given `TrackingExperiment` subclass.

## Backgrounds

The backgrounds directory contains tiff files representing the background image for a group of videos. In 2D tracking
experiments, the background is calculated from all videos for a given fish. In 3D tracking experiments, the background
is calculated for each video separately and these image files and contained within a subdirectory for each animal. The
name of the background file or subdirectory is the same as the fish ID.

## Tracking

Within the tracking directory, each animal gets its own folder with a name corresponding to the fish ID. For each video,
two tracking files are generated and stored in the fish subdirectory. These files are named using a unique video code
for each video in the experiment. The video code is the concatenation of fish ID and the video timestamp with non 
alpha-numeric characters removed. A typical video code has the form: YYYYMMDDNNHHMMSS.

Each video generates a .npy file containing xy coordinates of equally-spaced points along the tail for every frame. In
addition, there is a .csv file containing additional tracking information such as the fish centroid, heading, eye
centroids, eye angles, whether the tail struck the wall of the arena etc. This file also contains a column named
_tracked_ which indicates (bool) whether or not tracking data exists for a given frame.

## Kinematics

The kinematics directory has a similar structure to the tracking directory, with each fish getting its own subdirectory
and each video its own file. Here, there is single csv file for each video containing kinematic data for each frame now
represented in fish-centric coordinates. E.g. this file contains columns 'k0'->'kn' where k contains the angle of
intersection between the midline and a line drawn between adjacent pairs of tail points.

## Bouts

The _bouts.csv_ file contains all the detected putative bouts across all videos from all animals in an experiment. It 
has columns:
* ID
* video_code
* start
* end
* ROI

Where _ID_ is the fish ID, _video_code_ is the video, _start_ is the first frame of the bout, _end_ is the last frame of
the bout and _ROI_ specifies whether or not the tail contacted the wall of the arena at any point during the bout.

Note, this file does not necessarily contain all the bouts that were analyzed in the experiment. There is an additional
filtering step to remove spurious events prior to analysis. Analyzed bouts are contained within the analysis directory
of the experiment.

## Log

The _log.txt_ file is a simple log documenting the timeline of tracking and analysis in the experiment. It contains
information about when each step of the analysis was performed and keeps track of things like thresholds for detecting
bouts that are not contained within the metadata file. Logging can be turned off by passing False to the _log_ argument
when initializing a `TrackingExperiment` object.

## Analysis

Analysis is currently not entirely handled by `TrackingExperiment` objects, as analysis pipelines are more experiment-
specific than tracking. Some `TrackingExperiment` subclasses (such as `MappingExperiment`) do handle aspects of 
analysis, which are shared across multiple experiments.

For more details on analysis see the documentation in the [analysis module](../analysis/).

from ..analysis.bouts import transform_data, whiten_data, map_data
from ..miscellaneous import print_heading
import os
import numpy as np
import pandas as pd


def open_kinematics(events, kinematics_directory):
    # =========================
    # IMPORT ALL KINEMATIC DATA
    # =========================
    print_heading('IMPORTING DATA')
    # Initialise lists for storing DataFrames and indices
    kinematics_dfs = []
    multi_index_keys = []
    # Iterate through fish IDs
    for ID, fish_events in events.groupby('ID'):
        print ID
        print '-' * len(ID)
        # Iterate through video codes
        for video_code, video_events in fish_events.groupby('video_code'):
            print video_code
            # Open kinematics DataFrame
            kinematics_path = os.path.join(kinematics_directory, ID, video_code + '.csv')
            df = pd.read_csv(kinematics_path)
            # Assign frames to bouts
            event_indexer = np.zeros((len(df),), dtype='i4') - 1
            for idx, event_info in video_events.iterrows():
                event_indexer[event_info.start:event_info.end] = idx
            event_indexer = zip(event_indexer, df.index)
            df.index = pd.MultiIndex.from_tuples(event_indexer, names=['event_index', 'frame'])
            # Append DataFrame and indices to lists
            kinematics_dfs.append(df)
            multi_index_keys.append((ID, video_code))
        print ''
    # Create the multi-indexed data attribute
    data = pd.concat(kinematics_dfs, keys=multi_index_keys, names=['ID', 'video_code'])
    return data


class KinematicData(object):

    def __init__(self, data, metadata):
        self.data = data
        assert isinstance(self.data.index, pd.core.index.MultiIndex), 'Data must be MultiIndexed!'
        self.metadata = metadata

    @property
    def mean(self):
        return self.data.mean(axis=0)

    @property
    def std(self):
        return self.data.std(axis=0)

    def _slice_data(self, **kwargs):
        keys = kwargs.keys()
        # Create indexer for IDs and video codes
        indexer = dict(IDs=slice(None), video_codes=slice(None))
        for key in ['IDs', 'video_codes']:
            if key in keys and kwargs[key]:
                    indexer[key] = kwargs[key]
            elif key[:-1] in keys and kwargs[key[:-1]]:
                    indexer[key] = (kwargs[key[:-1]])
        # Slice IDs and video codes
        sliced = self.data.loc[(indexer['IDs'], indexer['video_codes']), :]
        # Slice bouts
        bout_index_values = sliced.index.get_level_values('event_index')
        for key in ('event_idxs', 'idxs', 'event_index', 'idx'):
            take_indices = kwargs.get(key)
            if take_indices is not None:
                if key[0] == 'i':
                    take_indices = bout_index_values.unique()[take_indices]
                sliced = sliced[np.isin(bout_index_values, take_indices)]
                break
        return sliced

    @staticmethod
    def _to_list(data, values=False, ndims=-1):
        if values:
            if ndims > 0:
                return [bout.reset_index(level=['ID', 'video_code', 'event_index'], drop=True).values[:, :ndims] for
                        bout_index, bout in
                        data.groupby('event_index')]
            else:
                return [bout.reset_index(level=['ID', 'video_code', 'event_index'], drop=True).values for
                        bout_index, bout in
                        data.groupby('event_index')]
        else:
            return [bout.reset_index(level=['ID', 'video_code', 'event_index'], drop=True) for
                    bout_index, bout in
                    data.groupby('event_index')]

    def get_event(self, ID=None, video_code=None, event_index=None, idx=0):
        bout = self._slice_data(ID=ID, video_code=video_code, bout_index=event_index, idx=idx)
        bout.reset_index(level=('ID', 'video_code', 'event_index'), drop=True, inplace=True)
        return bout

    def list_events(self, IDs=None, video_codes=None, event_idxs=None, idxs=None, values=False, ndims=-1):
        if all([IDs is None, video_codes is None, event_idxs is None, idxs is None]):
            return self._to_list(self.data, values=values, ndims=ndims)
        else:
            sliced = self._slice_data(IDs=IDs, video_codes=video_codes, event_idxs=event_idxs, idxs=idxs)
            return self._to_list(sliced, values=values, ndims=ndims)

    def whiten(self, mean=None, std=None):
        whitened = whiten_data(self.data, mean=mean, std=std)
        return KinematicData(whitened, metadata=self.metadata)

    def transform(self, whiten=True, mean=None, std=None):
        if whiten:
            data_to_transform = whiten_data(self.data, mean=mean, std=std)
        else:
            data_to_transform = self.data
        transformed, pca = transform_data(data_to_transform)
        transformed = pd.DataFrame(transformed, index=self.data.index)
        return KinematicData(transformed, metadata=self.metadata), pca

    def map(self, vectors, whiten=True, mean=None, std=None):
        if whiten:
            data_to_map = whiten_data(self.data, mean=mean, std=std)
        else:
            data_to_map = self.data
        mapped = map_data(data_to_map, vectors)
        mapped = pd.DataFrame(mapped, index=self.data.index, columns=self.data.columns)
        return KinematicData(mapped, metadata=self.metadata)


class JawData(KinematicData):

    def __init__(self, data, metadata=None):
        KinematicData.__init__(self, data, metadata)

    @classmethod
    def from_directory(cls, events, kinematics_directory, check_magnitude=True, elevation=False):
        # Open the bouts DataFrame
        if type(events) == pd.DataFrame:
            events_df = events
        elif type(events) == str:
            events_df = pd.read_csv(events, dtype={'ID': str, 'video_code': str})
        else:
            raise TypeError('bouts must be string or DataFrame')
        # Import kinematics data
        data = open_kinematics(events_df, kinematics_directory)
        data = data[data.index.get_level_values('event_index').isin(events_df.index)]
        if elevation:
            data = data[['depression', 'elevation', 'fish_elevation']]
        else:
            data = data[['depression', 'elevation']]
        jaw_data = cls(data, events_df)
        if check_magnitude:
            print 'Checking jaw movement magnitudes...',
            whitened = jaw_data.whiten()
            keep_events = []
            for event_index, event in whitened.data.groupby('event_index'):
                if np.all(event.max() < 10):
                    keep_events.append(event_index)
            keep_metadata = jaw_data.metadata.loc[keep_events]
            keep_data = jaw_data.data[jaw_data.data.index.get_level_values('event_index').isin(keep_events)]
            jaw_data = cls(keep_data, keep_metadata)
            print 'done!'
        return jaw_data

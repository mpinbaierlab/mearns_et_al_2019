Analysis 3D contains supplementary code for analysing behavior recorded from the side.

See also: [analysis](../analysis/) for the bulk of the analysis code.

## Jaw analysis

Contains the `JawData` class, used to handle and perform dimensionality reduction on jaw movements. Analogous to the 
`BoutData` class.

## Stimulus mapping

Analogous to its two-dimensional counterpart, `image_stimulus_map` subtracts a fish's pitch, centers the animal and 
crops the frame. Also contains additional `StimulusMapper` classes for analysing side data.

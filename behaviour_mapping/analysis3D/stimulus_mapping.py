from ..analysis.stimulus_mapping.stimulus_maps import StimulusMapper
from ..video import Video, video_code_to_timestamp
from ..tracking import background_division, crop_to_rectangle, rotate_and_centre_image, TrackingError
from ..miscellaneous import read_csv
from ..manage_files import get_files
import cv2
import numpy as np
from ast import literal_eval
import os
import pandas as pd


def image_stimulus_map(img, tracking_info):
    """Generates a map of the visual scene in fish-centred coordinates.

    Parameters
    ----------
    img : np.array
        Frame from video
    tracking_info : pd.Series or dict
        Tracking data for given frame

    Returns
    -------
    cropped : np.array
        Head-centred thresholded cropped image
    """

    blur = 255 - cv2.GaussianBlur(img.astype('uint8'), (3, 3), 0)
    threshed = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 7, 2)

    midpoint = tracking_info['head_midpoint']
    heading = tracking_info['heading']
    elevation = tracking_info['fish_elevation']
    midpoint = np.array(midpoint) / 2.
    if np.cos(heading) < 0:
        threshed = threshed[:, ::-1]
        midpoint[0] = threshed.shape[1] - midpoint[0]
        elevation *= -1
    else:
        elevation = elevation - np.pi

    centred = rotate_and_centre_image(255 - threshed, midpoint, elevation, 0)
    centre = np.array(centred.shape)[::-1] / 2
    cropped = crop_to_rectangle(centred, centre + (-150, 50), centre + (100, -50))
    return cropped


class VideoStimulusMapper3D():

    def __init__(self, video_path, background_path, tracking_path, ROI):
        self.video = Video(video_path)
        self.background = cv2.imread(background_path, 0)
        self.ROI = ROI
        self.columns = ['heading', 'head_midpoint', 'fish_elevation', 'side_tracked']
        self.tracking = read_csv(tracking_path, head_midpoint=self.convert_type)

    def generate_stimulus_map(self, f):
        self.video.frame_change(f)
        tracking_info = self.tracking.loc[f, self.columns].to_dict()
        if tracking_info['side_tracked']:
            img = self.video.grab_frame()[..., 0]
            subtracted = background_division(img, self.background)
            cropped = crop_to_rectangle(subtracted, *self.ROI)
            stim_map = image_stimulus_map(cropped, tracking_info)
            return stim_map
        else:
            raise TrackingError()

    def calculate_event_maps(self, events):
        video_maps = np.zeros((len(events), 101, 251), dtype='uint8')
        tracked = np.ones((len(events),), dtype='bool')
        i = 0
        for idx, event_info in events.iterrows():
            try:
                stim_map = self.generate_stimulus_map(event_info.bout_start)
                video_maps[i] = stim_map
            except TrackingError:
                tracked[i] = False
            i += 1
        video_maps = video_maps[tracked]
        tracked_events = events[tracked]
        return video_maps, tracked_events

    def calculate_random_maps(self, n_frames):
        video_maps = np.zeros((n_frames, 101, 251), dtype='uint8')
        random_frames = np.random.choice(self.tracking[self.tracking['side_tracked']].index, n_frames)
        for i, f in enumerate(random_frames):
            stim_map = self.generate_stimulus_map(f)
            video_maps[i] = stim_map
        return video_maps

    @staticmethod
    def convert_type(a):
        return np.array(literal_eval(a))


class JawStimulusMapper(StimulusMapper):

    def __init__(self, events, experiment, output_directory, **kwargs):
        StimulusMapper.__init__(self, experiment, output_directory, **kwargs)
        self.events = events

    def analyse_fish(self, ID):
        fish_events = self.events.groupby('ID').get_group(ID)
        fish_info = self.experiment.data[self.experiment.data['ID'] == ID].iloc[0]
        output_path = os.path.join(self.output_directory, ID + '.npy')
        events_path = os.path.join(self.output_directory, ID + '.csv')
        tracking_directory = os.path.join(self.experiment.directory, fish_info.tracking_directory)
        background_directory = os.path.join(self.experiment.directory, fish_info.background_directory)
        ROI = fish_info.side_ROI
        fish_maps = []
        tracked_events = []
        for video_code, video_events in fish_events.groupby('video_code'):
            video_file = video_code_to_timestamp(video_code)
            video_path = os.path.join(self.experiment.video_directory, fish_info.video_directory, video_file + '.avi')
            tracking_path = os.path.join(tracking_directory, video_code + '.csv')
            background_path = os.path.join(background_directory, video_code + '.tiff')
            mapper = VideoStimulusMapper3D(video_path, background_path, tracking_path, ROI)
            maps, tracked = mapper.calculate_event_maps(video_events)
            fish_maps.append(maps)
            tracked_events.append(tracked)
        fish_maps = np.concatenate(fish_maps, axis=0)
        tracked_events = pd.concat(tracked_events)
        np.save(output_path, fish_maps)
        tracked_events.to_csv(events_path, index=True)


class RandomStimulusMapper(StimulusMapper):

    def __init__(self, n_frames, experiment, output_directory, **kwargs):
        StimulusMapper.__init__(self, experiment, output_directory, **kwargs)
        self.n_frames = n_frames
        self.output_maps = {}

    def run(self):
        super(RandomStimulusMapper, self).run()
        all_maps = np.concatenate([maps for key, maps in self.output_maps.iteritems()], axis=0)
        np.save(os.path.join(self.output_directory, 'random_frames.npy'), all_maps)

    def analyse_fish(self, ID):
        fish_info = self.experiment.data[self.experiment.data['ID'] == ID].iloc[0]
        tracking_directory = os.path.join(self.experiment.directory, fish_info.tracking_directory)
        background_directory = os.path.join(self.experiment.directory, fish_info.background_directory)
        ROI = fish_info.side_ROI
        tracking_files, tracking_paths = get_files(tracking_directory, return_paths=True)
        tracking_pairs = [(f, path) for (f, path) in zip(tracking_files, tracking_paths) if f.endswith('.csv')]
        fish_maps = []
        for tracking_file, tracking_path in tracking_pairs:
            video_code, ext = os.path.splitext(tracking_file)
            video_file = video_code_to_timestamp(video_code)
            video_path = os.path.join(self.experiment.video_directory, fish_info.video_directory, video_file + '.avi')
            background_path = os.path.join(background_directory, video_code + '.tiff')
            mapper = VideoStimulusMapper3D(video_path, background_path, tracking_path, ROI)
            maps = mapper.calculate_random_maps(self.n_frames)
            fish_maps.append(maps)
        fish_maps = np.concatenate(fish_maps, axis=0)
        self.output_maps[ID] = fish_maps

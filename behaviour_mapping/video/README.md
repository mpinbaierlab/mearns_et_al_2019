The `Video` class handles avi video files using openCV. Video objects are created by passing the `Video` class a path to 
a video file. The class implements various useful methods such as `scroll` which allows the user to scroll through
frames in a simple GUI, `play` which plays the video in a window, and `return_frames` which returns a specified range of
frames from the video.

```python
from behaviour_mapping.video import Video

# Set the path to a avi file
my_path = 'D:\\Data\\my_video.avi'
# Create the video object
video = Video(my_path)

# Play the whole video at its native frame rate
video.play()
# Play frames 100-200 of the video at 20 fps
video.play(first_frame=100, last_frame=200, frame_rate=20)

# Scroll through the video using a track bar
video.scroll()
# Scroll through frames 100-200 using a track bar
video.scroll(first_frame=100, last_frame=200)

# Return frames 100-200 as a list
frames = video.return_frames(100, 200)
```

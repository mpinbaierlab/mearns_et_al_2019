# Analysis

The analysis package contains various modules for analysing behavior. It contains code for:
* Generating [behavioral spaces](#behavioral-space)
* Generating and analysing [transition matrices](#transitions)
* Paramecium tracking and calculating [stimulus maps](#stimulus-mapping)
* Analysing [eye convergence](#eye-convergence)
* [Miscellaneous](#miscellaneous) helper modules and functions

## Behavioral space

To generate a behavioral space, we perform isomap embedding of dynamic time warping distances between postural time
series. Code for implementing these various steps is divided between various packages and modules:

* [bouts](#bouts) - cleaning and dimensionality reduction of kinematic data
* [alignment](#alignment) - dynamic time warping (DTW), distance matrices etc.
* [embedding](#embedding) - isomap embedding

### Bouts

The bouts module contains the `BoutData` class which is used extensively throughout the analysis pipeline. This class
loads bouts extracted from kinematic files (see [tracking](../tracking/)) and contains methods 
for cleaning the data and performing dimensionality reduction.

`BoutData` objects are created by calling the `BoutData.from_directory` factory method. This takes a csv file containing
information about bouts pertaining to an experiment and a directory where the corresponding kinematic data are stored as 
arguments (see [experiments](../experiments/) for more details).

Kinematic data for all the imported bouts are stored as a multi-indexed pandas DataFrame in the `data` attribute of the
`BoutData` object. Individual bouts can be extracted easily from this DataFrame using the `get_bout` method, which
returns the kinematic data for a specified bout (bouts can be specified using animal IDs, video codes and indices).

The `list_bouts` method can be used to return a list of all bouts (each as its own DataFrame), or only bouts obtained
from a specified animal, video, or filtered by some other means by passing a list of indices.

Other useful methods implemented by this class include `whiten`, `transform` and `map`.  
* `whiten` normalizes the tail kinematic data but subtracting the mean tail shape over all frames and dividing by the
standard deviation. This ensures the diagonal of the covariance matrix is one when performing pca.
* `transform` calculates the principal components of the tail data from all bouts and maps the data onto these new basis 
vectors.
* `map` projects the tail data onto new pre-defined basis vectors. This is useful when mapping tracked tail data from
one experiment onto principal components calculated from tail data obtained in a different experiment.

### Alignment

#### Dynamic time warping
Alignment contains functions for implementing dynamic time warping (DTW) to calculate the similarity between pairs of
bouts (see [Jouary and Sumbre, 2016](https://www.biorxiv.org/content/10.1101/052324v1)). DTW finds the optimal alignment 
of two time series that minimizes a cost function. The cost function we use is the sum of Euclidean distances between 
aligned points in the two time series. The alignment algorithm is implemented in the `dtw` function, which takes two 
time series (_s_ and _t_) and returns the minimum sum of Euclidean distances between them after warping within a window 
defined by the _bandwidth_.

For aligning one-dimensional time series, `dtw_1d` is used instead. Here, the cost function is the absolute difference
between values in the time series.

#### Distances

To generate the behavioral space, DTW distances must be calculated between every pair of bouts in the data set. This is
implemented in the function `calculate_distance_matrix`. Since the distance matrix is expected to be symmetric, only the
upper or lower triangle is required. `calculate_distance_matrix` fills the triangle row-by-row and runs in multiple CPUs
using the [joblib](https://joblib.readthedocs.io/en/latest/parallel.html) library. The result is returned as a condensed
matrix. The time taken to calculate the distance matrix scales with the square of the number of bouts.

For aligning one set of bouts to another set of bouts (e.g. when mapping one dataset to the exemplars of another) the
function `calculate_distance_matrix_templates` is used instead. This is significantly faster than 
`calculate_distance_matrix` as the time scales linearly with the number of bouts to be aligned.

### Embedding

The embedding module is used to generate a behavioral space from a distance matrice using isomap embedding (see
[Tenenbaum et al., 2000](https://science.sciencemag.org/content/290/5500/2319.abstract)). 
`IsomapPrecomputed` calculates the isomap embedding of a pre-computed distance matrix. The embedding vectors are the
eigenvectors of a graph distance matrix.

## Transitions

This contains functions for calculating transition frequency matrices between bout types and unsupervised analysis of
such matrices using singular-value decomposition.

* `transition_matrix` takes a sequence of states and calculates the number of transitions between every pair of states.
* `fish_transition_matrices` calculates the transition frequency matrix for each fish in an experiment. It does not
   allow transitions to occur across video boundaries.
* `SVD` finds the singular-value decomposition of the symmetric and anti-symmetric parts of a matrix.
* The reweighting module contains two functions:
    * `generate_weights` calculates a weight matrix from a set of points, where weights are a decaying exponential
       function of the distance between pairs of points.
    * `redistribute_transitions` redistributes the transitions between pairs of states in a transition frequency matrix
       to nearby states in behavioural space. This is achieved by the calculation: _WTW<sup>T</sup>_, where _T_ is the
       transition matrix and _W_ is a weight matrix whose columns sum to one.
* The `CrossValidateSVD` class implements a cross-validated analysis of transition modes using SVD. It partitions data
  into training and test sets and builds a range of models of expected transitions by summing transition modes obtained
  from the training data. Models are compared to the test data by calculating the sum of square errors between matrices.

## Stimulus mapping

Stimulus mapping contains code for tracking paramecia in videos and calculating prey probability density maps.

Basic functions found in this package include:
* `find_paramecia`, which finds putative prey objects in a frame from a video
* `image_stimulus_map`, which rotates and head-centers a fish, and then crops a frame from a video

Maps of prey locations are calculated using various `StimulusMapper` classes, which are subsequently processed and
analyzed in different scripts.

## Eye convergence

Eye convergence analysis is performed using two classes.

Data for each fish is processed independently using the 
`EyeConvergenceAnalyser` class. This class can do various useful things with eye tracking data, such as:
* Performing `kernel_density_estimation` of the eye convergence data
* Finding a threshold for defining eye convergence in an unbiased manner (`find_convergence_threshold`)
* Finding the proportion of time an animal spends with its eyes converged as a proxy for prey capture
  (`calculate_convergence_score`)
* Numerous plotting methods to visualize the eye convergence distribution for an animal

Eye convergence analysis for all fish in an experiment can be performed using the `EyeTrackingData` class. Data can be
imported by calling `EyeTrackingData.from_experiment(path)`, where _path_ is the path to the experiment directory (see 
[experiments](../experiments/)). With `calculate_convergence_scores`, you can calculate the
convergence scores for each fish in an experiment. `calculate_convergence_distribution` and 
`calculate_angle_distribution` calculate the one-dimensional distribution of eye convergence angles, and two-dimensional
distribution of left and right eye angles, respectively, over all fish in an experiment.

## Miscellaneous

### Clustering

Implements simple clustering steps deployed in the analysis pipeline. Includes: 
* `affinity_propagation` used to identify exemplar bouts
* `hierarchical_clustering` used to identify behavioral clusters

### Interpolation

The function `interpolate_nd` takes an n-dimensional time series obtained with a given sampling frequency, _fs_, and
interpolates points to fit a new sampling frequency, _fs_new_. This is used when comparing bouts from different datasets
that were acquired at different frame rates.

### Plotting helpers

Defunct: re-implemented in [plotting](../../plotting/).

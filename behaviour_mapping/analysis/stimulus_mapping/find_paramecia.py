from behaviour_mapping.tracking import contour_info
import cv2
import numpy as np
from scipy import ndimage


def find_paramecia(img, image_threshold=5, size_threshold=3):
    """Finds putative paramecia within a stimulus map"""
    filt = ndimage.median_filter(img, 5)
    threshed = ndimage.grey_erosion(filt, 3) > image_threshold
    threshed = threshed.astype('uint8') * 255
    im2, contours, hierarchy = cv2.findContours(threshed, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    centres, orientations = zip(*[contour_info(cntr) for cntr in contours if cv2.contourArea(cntr) > size_threshold])
    centres = np.array(centres)
    orientations = np.array(orientations)
    return centres, orientations

The tracking package is used for animal tracking. It implements machine vision algorithms to track zebrafish larvae in
the standard 2D behavior assay, as well as some helper functions for displaying the tracking output.

Modules:
* [background](#background)
* [tracking](#tracking)
* [kinematics](#kinematics)
* [bouts](#bouts)
* [region_of_interest](#region-of-interest)
* [display](#display)
* [tail_fit](#tail-fit)

## Background

The background module contains functions for calculating background images from videos and implementing background
subtraction. Important functions contained in this module are `calculate_projection` which implements various kinds of
background calculation for videos (e.g. median, mean, maximum) and `background_division` which implements a background
subtraction and normalization.

## Tracking

The tracking module implements the heavy-lifting functions for tracking animals in each frame of a video. For example,
the `analyse_frame` function takes an raw frame from a video, a background image and various thresholds as inputs and 
returns tracking data for that frame, such as points fitted to the tail and eye positions and angles. This module also
implements a `TrackingError` exception which is raised whenever tracking fails on a given frame for whatever reason.

## Kinematics

The kinematics module contains functions that take raw tracked postural variables and converts them to kinematic data in 
fish-centered coordinates. For example, it contains the `calculate_tail_curvature` function that takes tail points in 2D 
coordinate space and converts it to 1D tail angle data. The `calculate_kinematics` function takes paths to raw tracking 
variables, converts them to kinematic variables, and saves the output.

## Bouts

The bouts module contains functions for segmenting the behavior of zebrafish larvae into putative bouts. For example,
`find_bouts` implements a simple bout detection algorithm that finds frames in which the smoothed absolute derivative of
tail angles is above a pre-defined threshold. This module also contains a `SetBoutDetectionThreshold` class which
implements a simple GUI for setting thresholds that can be passed to the `find_bouts` function.

## Region of interest

For some analysis steps, it is important to check that the tail of the fish falls within a pre-determined region of
interest. For example, in our analysis of zebrafish behavior we excluded bouts in which the tail struck the wall of the
arena, as this could introduce deformations in the tail shape that are not purely a result of the fish's own motion. In
addition, we use ROIs to determine the top and side views in 3D behavioral data acquired using a single camera. The 
region of interest module contains the `RegionOfInterest` class which allows the user to define a rectangular ROI within
an image, and the `check_points_in_ROI` function which checks that all tail points fall within a given ROI.

## Display

The display module contains functions that assist the user in visualizing the tracking output (such as `set_thresholds`, 
which opens a GUI for setting thresholds for tracking) as well as the `rotate_and_centre_image` function which can be
used to generate head-stabilized images and videos.

## Tail fit

The tail fit module contains low-level functions for tracking the tail of the fish in a single frame from a video. Our
tail tracking algorithm is different from those used in other studies (see, for example,
[Burgess & Granato, 2007](https://jeb.biologists.org/content/210/14/2526);
[Severi et al., 2014](https://www.sciencedirect.com/science/article/pii/S0896627314005789);
[Marques et al., 2018](https://www.sciencedirect.com/science/article/pii/S0960982217316044?via%3Dihub)).

To fit points to the tail, we identify the largest blob in the image using background subtraction, thresholding and
contour detection. This blob is then skeletonized using `skimage.morphology.skeletonize`. Within this skeleton, we
identify the longest path that (1) begins at the point nearest the centre of the swim bladder and (2) travels in a
direction opposite to the heading of the fish. This is implemented by the functions `walk_skeleton` (slower, but
thoroughly debugged) and `fast_longestpath` (faster, but not completely debugged). We then interpolate points along this
path to obtain the appropriate number of evenly spaced tail points.
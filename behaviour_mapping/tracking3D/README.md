Tracking 3D contains supplementary code for tracking larvae that have been recorded simultaneously from above and from 
the side.

See also: [tracking](../tracking/) for the bulk of the tracking code.

## Tracking helpers 3D

### Jaw tracking

The function `find_jaw_point` is used to determine where a given _vector_ extended from a given _point_ point intersects
the contour of the fish (_fish_mask_). It is used to find the distance to the base of the jaw in a fish viewed from the
side.

### Tracking 3D

The functions `analyse_frame_3d`, `set_thresholds_3d`, `track_video_3d`, `show_tracking_3d` and `check_tracking_3d` are
all re-implementations of their two-dimensional counterparts that include tracking fish from the side. Note that 
side-tracking data only exists if the orientation of the fish is within 45<sup>o</sup> of the imaging place.

### Kinematics 3D

The function `calculate_kinematics_3d` is a re-implementation of its two-dimensional counterpart. It includes extracting
kinematics from tracked side and jaw data, including the pitch of the fish in the water, elevation of the cranium, and
depression of the lower jaw.

## Jaw movements

Contains functions for identifying jaw movements. `find_jaw_movements` identifies jaw movements within a given section
of kinematic data extracted from a video by finding frames where the jaw depression is above a pre-defined threshold.
